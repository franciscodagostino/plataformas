﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class Reset : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("cierra"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}
