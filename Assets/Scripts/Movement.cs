﻿using UnityEngine;

public class Movement : MonoBehaviour
{

    public Rigidbody2D rb;
    public float groundRadius = 0.2f;

    public float moveSpeed = 10f;
    public float jumpForce = 10f;
    public Transform groundCheck;
    public LayerMask Ground;
    bool isGrounded = false;


    public float wallJumpTime = 0.7f;
    public float wallDistance = 0.55f;
    bool isWallStanding = false;
    RaycastHit2D wallCheckHit;
    float jumpTime;
    

    float mx = 0f;
    bool isFacingRight = true;
    public float wallJumpCounter;
    private int powerUpWallJump;



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("powerUpWallJump"))
        {
            powerUpWallJump++;
            other.gameObject.SetActive(false);
        }
    }



    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("pisoFalso"))
        {
            other.gameObject.SetActive(false);
        }
    }


    private void Update()
    {
        if (wallJumpCounter <= 0)
        {
            mx = Input.GetAxis("Horizontal");

            if (mx < 0)
            {
                isFacingRight = false;
                transform.localScale = new Vector2(-1, transform.localScale.y);
            }
            else
            {
                isFacingRight = true;
                transform.localScale = new Vector2(1, transform.localScale.y);
            }

            rb.velocity = new Vector2(mx * moveSpeed, rb.velocity.y);

            bool touchingGround = Physics2D.OverlapCircle(groundCheck.position, groundRadius, Ground);

            if (touchingGround)
            {
                isGrounded = true;
            }
            else
            {
                isGrounded = false;
            }
            if (isGrounded && Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
            if (Input.GetKeyUp(KeyCode.Space) && rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
            }

            if (powerUpWallJump == 1) {
                if (isFacingRight)
                {
                    wallCheckHit = Physics2D.Raycast(transform.position, new Vector2(wallDistance, 0), wallDistance, Ground);

                }
                else
                {
                    wallCheckHit = Physics2D.Raycast(transform.position, new Vector2(-wallDistance, 0), wallDistance, Ground);
                }
                if (wallCheckHit && !isGrounded && mx != 0)
                {
                    isWallStanding = true;

                }
                else if (jumpTime < Time.time)
                {
                    isWallStanding = false;
                }
                if (isWallStanding)
                {
                    rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -.4f, float.MaxValue));
                }
                
                if (isWallStanding && Input.GetKeyDown(KeyCode.Space))
                {
                    wallJumpCounter = wallJumpTime;
                    WallJump();
                } 
            }
        }
        else
        {
            wallJumpCounter -= Time.deltaTime;
        }

    }



    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }
    void WallJump()
    {
        rb.velocity = new Vector2(-Input.GetAxisRaw("Horizontal") * moveSpeed, jumpForce);
    }



}
