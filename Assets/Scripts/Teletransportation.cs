﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teletransportation : MonoBehaviour
{

    public GameObject portalS;
    private GameObject playerOne;

    private void Start()
    {
        playerOne = GameObject.FindGameObjectWithTag("PlayerU");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
      playerOne.transform.position = portalS.transform.position - new Vector3(2, 0, 0) + new Vector3(0, playerOne.transform.position.y, 0);
    }



}
