﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tp3 : MonoBehaviour
{
    public GameObject postTp;
    private GameObject playerOne;

    private void Start()
    {
        playerOne = GameObject.FindGameObjectWithTag("PlayerU");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        playerOne.transform.position = postTp.transform.position;
    }
}
