﻿using UnityEngine;

public class CameraControl : MonoBehaviour
{

    private GameObject Player;

    public Vector3 offset;
    public Vector3 CamY;
    
    

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("PlayerU");
        offset = transform.position - Player.transform.position;
        CamY = new Vector3(0, 4, 0);
    }

    
    void LateUpdate()
    {


        transform.position = new Vector3(transform.position.x, Player.transform.position.y, transform.position.z) + CamY;
    }
    

}
